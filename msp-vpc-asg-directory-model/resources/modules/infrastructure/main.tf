module "vpc" {
  source      = "./vpc"

  environment = var.environment
  region      = var.region
}
module "subnets" {
  source      = "./subnets"

  environment = var.environment
  region      = var.region
  vpc_id      = module.vpc.vpc_id
  availability_zone_single = var.availability_zone_single
}
module "asg" {
  source = "./asg"
  
  environment = var.environment
  region      = var.region
  ami_id      = var.ami_id
  availability_zone_names = var.availability_zone_names
  asg_scale_out_time = var.asg_scale_out_time
  asg_scale_in_time  = var.asg_scale_in_time
  subnet_id   = module.subnets.public_subnet_id
  availability_zone_single = var.availability_zone_single
}
