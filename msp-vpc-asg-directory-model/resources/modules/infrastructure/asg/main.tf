resource "aws_launch_template" "launch_template" {
  name_prefix   = "${var.environment}-${var.region}"
  image_id      = var.ami_id
  instance_type = "t2.micro"
  network_interfaces {
    associate_public_ip_address = true
    subnet_id                   = var.subnet_id
    delete_on_termination       = true
  }
  placement {
    availability_zone = var.availability_zone_single
  }
}

resource "aws_autoscaling_group" "auto_scaler" {
  availability_zones = var.availability_zone_names
  name               = "${var.environment}-${var.region}-AutoScalingGroup"
  desired_capacity   = 3
  max_size           = 3
  min_size           = 1

  launch_template {
    id      = "${aws_launch_template.launch_template.id}"
    version = "$Latest"
  }
}
resource "aws_autoscaling_schedule" "scale_out_schedule" {
  scheduled_action_name  = "Scale-Out-${var.environment}"
  min_size               = 1
  max_size               = 3
  desired_capacity       = 3
  recurrence             = "${var.asg_scale_out_time}"
  autoscaling_group_name = "${aws_autoscaling_group.auto_scaler.name}"
}
resource "aws_autoscaling_schedule" "scale_in_schedule" {
  scheduled_action_name  = "Scale-In-${var.environment}"
  min_size               = 0
  max_size               = 1
  desired_capacity       = 0
  recurrence             = "${var.asg_scale_in_time}"
  autoscaling_group_name = "${aws_autoscaling_group.auto_scaler.name}"
}