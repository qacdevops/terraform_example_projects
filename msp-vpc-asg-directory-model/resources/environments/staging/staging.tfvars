#Operates in Mumbai
environment  = "staging"
region       = "ap-south-1"
access_key   = "<your-access-key>"
secret_key   = "<your-secret_key>"
ami_id       = "ami-0217a85e28e625474"
availability_zone_names = ["ap-south-1a"]
availability_zone_single = "ap-south-1a"
asg_scale_out_time = "30 14 * * MON-FRI"
asg_scale_in_time  = "30 22 * * MON-FRI"