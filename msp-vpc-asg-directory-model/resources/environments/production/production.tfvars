#Operates in London
environment  = "production"
region       = "eu-west-2"
access_key   = "<your-access-key>"
secret_key   = "<your-secret_key>"
ami_id       = "ami-0089b31e09ac3fffc"
availability_zone_names = ["eu-west-2a"]
availability_zone_single = "eu-west-2a"
asg_scale_out_time = "0 9 * * MON-FRI"
asg_scale_in_time  = "0 17 * * MON-FRI"