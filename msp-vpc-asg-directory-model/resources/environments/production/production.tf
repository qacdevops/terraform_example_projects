variable "environment" {
}
variable "region" {
}
variable "access_key" {
}
variable "secret_key" {
}
variable "ami_id" {
}
variable "availability_zone_names" {
}
variable "asg_scale_out_time" {
}
variable "asg_scale_in_time" {
}
variable "availability_zone_single" {
}
#Ideally you wouldn't provide your keys in terraform, running 'aws configure' or setting environment variables is a better and more secure alternative
provider "aws" {
   region = var.region
   version = "~> 2.7"
   access_key = var.access_key
   secret_key = var.secret_key
}
module "infrastructure" {
   source      = "../../modules/infrastructure"
   environment = var.environment
   region      = var.region
   ami_id      = var.ami_id
   availability_zone_names = var.availability_zone_names
   asg_scale_out_time = var.asg_scale_out_time
   asg_scale_in_time  = var.asg_scale_in_time
   availability_zone_single = var.availability_zone_single
}
