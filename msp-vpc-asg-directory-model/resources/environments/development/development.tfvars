#Operates in Paris
environment  = "development"
region       = "eu-west-3"
access_key   = "<your-access-key>"
secret_key   = "<your-secret_key>"
ami_id       = "ami-007fae589fdf6e955"
availability_zone_names = ["eu-west-3a"]
availability_zone_single = "eu-west-3a"
asg_scale_out_time = "0 10 * * MON-FRI"
asg_scale_in_time  = "0 18 * * MON-FRI"